# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Journey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('dep_date', models.DateField(default=datetime.datetime(2019, 4, 21, 16, 26, 1, 786666))),
                ('arr_date', models.DateField(default=datetime.datetime(2019, 4, 22, 16, 26, 1, 786666))),
                ('city', models.ForeignKey(related_name='jcity', to='trip.City')),
                ('country', models.ForeignKey(related_name='jcountry', to='trip.Country')),
            ],
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(related_name='country', to='trip.Country'),
        ),
    ]
