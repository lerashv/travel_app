from django.db import models
from datetime import datetime, timedelta

class Country(models.Model):
    name = models.CharField(max_length=64)   

    def __str__(self):
        return f"{self.country}, {self.city}"

class City(models.Model):
    country = models.ForeignKey('Country', on_delete=models.CASCADE, related_name="country")
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name} in {self.country}"

class Journey(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE, related_name="jcountry")
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name="jcity")
    dep_date = models.DateField(default=datetime.now())
    arr_date = models.DateField(default=datetime.now()+timedelta(days=1))

    def __str__(self):
        return f"{self.country}, {self.city} from {self.dep_date} till {self.arr_date}"