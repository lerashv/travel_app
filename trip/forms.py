from django import forms

class CountryForm(forms.Form):
    country = forms.CharField(label="Country to go", max_length=60)