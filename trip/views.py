from django.shortcuts import render

from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout

from .models import Country, City, Journey
from .forms import CountryForm

def index(request):
    if not request.user.is_authenticated:
        form = CountryForm() 
        context = {
            'form':form,
            'message': "login"
        }
        return render(request, 'trip/index.html', context)
    else:
        form = CountryForm() 
        return render(request, 'trip/index.html', {'form': form})    