from django.shortcuts import render

from django.contrib.auth.forms import UserCreationForm

def reg(request):
    return render(request, 'users/register.html', {'form': UserCreationForm})

# TODO: handle user registration , add authentication form